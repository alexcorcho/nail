import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( public afAuth: AngularFireAuth, private router: Router, private AuthService :AuthService) { }
    public email:string = '';
    public password: string ='';

  ngOnInit() {
  }

  onLogin(): void {
    this.AuthService.loginEmailUsuario(this.email, this.password)
    .then((res) => {
      this.router.navigate(['admin/servicios']);
    }).catch (err => console.log('err', err.message));
  }

  onLoginGoogle(): void {
    this.AuthService.loginGoogleUsuario()
    .then((res) => {
      console.log('resUser', res);
      this.router.navigate(['admin/servicios']);
      }).catch(err => console.log('err' , err.message));
  }

  onLoginFacebook():void{
    this.AuthService.loginFacebookUsuario()
    .then((res) => {
      console.log('resUser', res);
      this.router.navigate(['admin/servicios']);
      }).catch(err => console.log('err' , err.message));
  }




}
