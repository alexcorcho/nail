import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService} from '../../services/auth.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private AuthService: AuthService, private afsAuth: AngularFireAuth, private router: Router) { }
  public app_name:string = 'NAIL';
  public estaLogiado:boolean = false;
  ngOnInit() {

    this.observador();
  }

  observador(){
    this.AuthService.isAuth().subscribe( auth => {
      if (auth){
        // si esta logiado
        this.estaLogiado = true;
      } else {
        // y si no
        this.estaLogiado = false;
      }
    } );

  }

  onLogout(){
    this.AuthService.logoutUsuario();
  }
 
}
