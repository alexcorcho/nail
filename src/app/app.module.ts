import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ServiciosComponent } from './componentes/admin/servicios/servicios.component';
import { DetalleServicioComponent } from './componentes/detalle-servicio/detalle-servicio.component';
import { HeroComponent } from './componentes/hero/hero.component';
import { ModalComponent } from './componentes/modal/modal.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { OfertasComponent } from './componentes/ofertas/ofertas.component';
import { LoginComponent } from './componentes/usuario/login/login.component';
import { PerfilComponent } from './componentes/usuario/perfil/perfil.component';
import { RegistroComponent } from './componentes/usuario/registro/registro.component';
import { Pagina404Component } from './componentes/usuario/pagina404/pagina404.component';
import { FormsModule } from '@angular/forms'; 
import { HomeComponent } from './componentes/home/home.component';
import { AngularFireAuth } from '@angular/fire/auth';



@NgModule({
  declarations: [
    AppComponent,    
    ServiciosComponent,
    DetalleServicioComponent,
    HeroComponent,
    ModalComponent,
    NavbarComponent,
    OfertasComponent,
    LoginComponent,
    PerfilComponent,
    RegistroComponent,    
    Pagina404Component,    
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
     AngularFirestoreModule,
     FormsModule,
     AngularFireDatabaseModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
