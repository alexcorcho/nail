import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './componentes/home/home.component';
import { OfertasComponent } from './componentes/ofertas/ofertas.component';
import { DetalleServicioComponent } from './componentes/detalle-servicio/detalle-servicio.component';
import { ServiciosComponent } from './componentes/admin/servicios/servicios.component';
import { LoginComponent } from './componentes/usuario/login/login.component';
import { RegistroComponent } from './componentes/usuario/registro/registro.component';
import { PerfilComponent } from './componentes/usuario/perfil/perfil.component';
import { Pagina404Component } from './componentes/usuario/pagina404/pagina404.component';


const routes: Routes = [  
{ path: '', component: HomeComponent },
{ path: 'ofertas', component: OfertasComponent},
{ path: 'servicios/id', component: DetalleServicioComponent },
{ path: 'admin/servicios', component: ServiciosComponent},
{ path: 'usuario/login', component: LoginComponent},
{ path: 'usuario/registro', component: RegistroComponent},
{ path: 'usuario/perfil', component: PerfilComponent},
{path: ' **' , component: Pagina404Component}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
