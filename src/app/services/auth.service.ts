import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { auth } from 'firebase';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private afsAuth: AngularFireAuth, private router: Router) { }

  registroUsuario(){}
  loginEmailUsuario(email:string, pass:string){
    return new Promise ((resolve, reject)=>{
      this.afsAuth.auth.signInWithEmailAndPassword(email,pass)
      .then(userData => resolve(userData),
      err => reject (err));
    });
  }
  loginFacebookUsuario(){
   return this.afsAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
    
  }
  loginGoogleUsuario(){
  return  this.afsAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    
  }
  logoutUsuario(){
   return this.afsAuth.auth.signOut();
  }

  //observador
  isAuth(){
    return this.afsAuth.authState.pipe(map(auth => auth));
  }
}
